from setuptools import setup

setup(
    name = "randomboy",
    version = "0.1.0",
    description = "Randomizer that is random",
    author = "AitzazImtiaz",
    url = "https://github.com/AitzazImtiaz/Boydom-OS",
    packages = ["randomboy"],
    entry_points = {
        'console_scripts': [
            'randomboy = randomboy.__main__:main'
        ]
    },
)
