import random
import string
from colorama import init, Fore, Style

def main():
    # Initialize colorama
    init()

    while True:
        # Generate a random ASCII character
        # Generate a random character from the extended ASCII character set
        random_char = chr(random.randint(128, 255))

        # Generate a random foreground color
        random_color = random.choice([Fore.RED, Fore.GREEN, Fore.YELLOW, Fore.BLUE, Fore.MAGENTA, Fore.CYAN, Fore.WHITE])

        # Output the random character in the random color
        print(random_color + random_char + Style.RESET_ALL, end="")
