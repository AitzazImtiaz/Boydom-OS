from setuptools import setup

setup(
    name = "megangirl",
    version = "0.1.0",
    description = "The first AI girl",
    author = "AitzazImtiaz",
    url = "https://github.com/AitzazImtiaz/Boydom-OS",
    packages = ["megangirl"],
    entry_points = {
        'console_scripts': [
            'megangirl = megangirl.__main__:main'
        ]
    },
)
