import cleverbotfreeapi
import os
import subprocess

def main():
  while True:
    inp = subprocess.getoutput("termux-speech-to-text")
    out = cleverbotfreeapi.cleverbot(inp, session="Deftera")
    fout = f"termux-tts-speak '{out}'"
    os.system(fout)
