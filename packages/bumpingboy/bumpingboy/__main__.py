import math

def a(n):
    ceil_sqrt_n = math.ceil(math.sqrt(n))
    x = n - (ceil_sqrt_n * (ceil_sqrt_n - 1))
    power = abs(x - 1)
    result = (n + 1) * (2 ** power) / 2
    return result

def main():
  b = 0
  while True:
    print(a(b))
    print("\n")
    b = b + 1
  
