from setuptools import setup

setup(
    name = "bumpingboy",
    version = "0.1.0",
    description = "A jumping version for jumpy things!",
    author = "AitzazImtiaz",
    url = "https://github.com/AitzazImtiaz/Boydom-OS/packages/bumpingboy",
    packages = ["bumpingboy"],
    entry_points = {
        'console_scripts': [
            'bumpingboy = bumpingboy.__main__:main'
        ]
    },
)
