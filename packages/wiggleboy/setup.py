from setuptools import setup

setup(
    name = "wiggleboy",
    version = "0.1.0",
    description = "A version to wiggle on",
    author = "AitzazImtiaz",
    url = "https://github.com/AitzazImtiaz/Boydom-OS",
    packages = ["wiggleboy"],
    entry_points = {
        'console_scripts': [
            'wiggleboy = wiggleboy.__main__:main'
        ]
    },
)
