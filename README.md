# Boydom OS
A new Termux command line OS based on packages that run to perform various features.
   
# Installation
For first time users:

    git clone https://github.com/AitzazImtiaz/Boydom-OS
    cd Boydom-OS
    chmod +x install.sh
    ./install.sh
    exit
    wait for system to run then
    Type helpboy to get commands

Report any issues or features request in the Issues section.
# Updating and Contributing
Boydom comes with new packages everytime, and in future, will seemingly get more packages, to get in touch with those type:
```
updateboy
```
and to see and understand every package, you can use:
```
helpboy
```
# Updates
* Added a Voice start!
* Added command line tools
